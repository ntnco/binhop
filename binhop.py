from random import randint
from threading import Thread
from time import time


def get_input(prompt, timeout):
    inp = [None]
    def user_input(r):
        r[0] = input(prompt)
    user = Thread(target=user_input, args=(inp,))
    user.daemon = True
    user.start()
    user.join(timeout)
    return inp[0]

def start():
    print("Convert binary numbers to decimal.")
    return time()

def finish(last_number, score, total):
    print("\nTime is up! The last number was", last_number,
            "\nYour final score is ", 
            str(score) + "/" + str(total))

def get_pair():
    num = randint(0, 255)
    binum = format(num, '08b')
    return num, binum

def feedback(ans, correct):
    if ans == correct:
        print("yes!\n")
        score = 1
    else:
        print("no. Correct is", correct, "\n")
        score = 0
    return score

def play():
    start_time = start()
    score = total = 0
    nums, bins = None, None
    ans = 1
    last_number = -1
    while ans is not None:
        num, binum = get_pair()
        seconds_passed = (time() - start_time)
        time_left = 30 - seconds_passed
        print(format(time_left, ".1f") + "s left")
        ans = get_input(binum + "\n", time_left)
        if ans:
            score += feedback(ans, str(num))
            total += 1
        last_number = num
    finish(last_number, score, total)

try:
    play()
except KeyboardInterrupt:
    print("\nGame interrupted")
