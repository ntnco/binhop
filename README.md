# binhop
Practice converting bytes to decimal.

## Installation

1. Clone this repo locally. For example by ssh: `git clone git@gitlab.com:ntnco/binhop.git`
2. Go inside the folder: `cd binhop`
3. Run the game: `python3 binhop.py`
4. You have 30 seconds to submit as many 0-255 integers as possible
